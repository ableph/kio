#include "httpprotocoltype.h"

HTTPProtocolType::HTTPProtocolType(const QString &protocol)
    : m_name(protocol)
{
}

HTTPProtocolType::HTTPProtocolType(const QByteArray& protocol)
: HTTPProtocolType(QString::fromLatin1(protocol))
{
}

bool HTTPProtocolType::isDav() const {
    return isDav(m_name);
}

bool HTTPProtocolType::isEncrypted() const {
    return isEncrypted(m_name.toLatin1());
}

bool HTTPProtocolType::isEncrypted(const QByteArray& protocol) {
    return protocol.endsWith('s');
}

bool HTTPProtocolType::isDav(const QString& p) {
    return p.startsWith(QLatin1String("webdav"))
        || p.startsWith(QLatin1String("dav"));
}

QString HTTPProtocolType::asEncrypted(bool encrypted) const {
    auto pn = m_name;
    if (encrypted && !pn.endsWith(QLatin1Char('s'))) {
        pn += QLatin1Char('s');
    }
    else if (!encrypted && pn.endsWith(QLatin1Char('s'))) {
        pn.chop(1);
    }
    return pn;
}

QUrl HTTPProtocolType::protocolChangedToHttp(const QUrl &url)
{
    QUrl newUrl{url};
    QString protocol{newUrl.scheme()};
    protocol.replace(QLatin1String{"webdav"}, QLatin1String{"http"});
    protocol.replace(QLatin1String{"dav"}, QLatin1String{"http"});
    if (newUrl.scheme() != protocol)
        newUrl.setScheme(protocol);
    return newUrl;
}

void HTTPProtocolType::changeHttpToProtocol(QUrl& url) const {
    if (!url.scheme().startsWith(QLatin1String("http")))
        return;
    // get our protocolname, but use the redirect's notion of encryption
    auto newScheme = url.scheme();
    auto protocolName = asEncrypted(false);
    newScheme.replace(QLatin1String{"http"}, protocolName);

    url.setScheme(newScheme);
}
