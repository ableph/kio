/*
 *    SPDX-FileCopyrightText: 2022 Alexander Busse <alex@busse.earth>
 *
 *    SPDX-License-Identifier: LGPL-2.0-or-later
 */
#ifndef HTTPPROTOCOLTYPE_H_INCLUDED
#define HTTPPROTOCOLTYPE_H_INCLUDED

#include <QUrl>
struct HTTPProtocolTypeTest;

struct HTTPProtocolType {
    explicit HTTPProtocolType(const QString &protocol);
    explicit HTTPProtocolType(const QByteArray &protocol);

    bool isEncrypted() const;
    bool isDav() const;
    static bool isEncrypted(const QByteArray &protocol);
    static bool isDav(const QString &p);
    // replace our protocol name with http
    Q_REQUIRED_RESULT static QUrl protocolChangedToHttp(const QUrl &url);

    // If we're redirected to a http:// url, remember that we're doing webdav and also remember whether we use ssl
    void changeHttpToProtocol(QUrl &url) const;

    bool operator==(const QString &s) const
    {
        return m_name == s;
    }
    operator const QString &() const
    {
        return m_name;
    }
    operator const QByteArray() const
    {
        return m_name.toLatin1();
    }

private:
    friend HTTPProtocolTypeTest;
    const QString m_name;
    QString asEncrypted(bool encrypted = true) const;

};

#endif // HTTPPROTOCOLTYPE_H_INCLUDED
