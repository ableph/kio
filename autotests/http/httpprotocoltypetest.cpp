#include "httpprotocoltypetest.h"
#define _TEST_HTTPPROTOCOLTYPE_
#include "httpprotocoltype.h"

#include <QTest>

QTEST_GUILESS_MAIN(HTTPProtocolTypeTest);


void HTTPProtocolTypeTest::testIsDav()
{
    QVERIFY(HTTPProtocolType::isDav("dav"));
    QVERIFY(HTTPProtocolType::isDav("davs"));
    QVERIFY(HTTPProtocolType::isDav("webdav"));
    QVERIFY(HTTPProtocolType::isDav("webdavs"));
    QVERIFY(!HTTPProtocolType::isDav("http"));
    QVERIFY(!HTTPProtocolType::isDav("https"));
}

void HTTPProtocolTypeTest::testIsEncrypted()
{
    QVERIFY(!HTTPProtocolType::isEncrypted("dav"));
    QVERIFY(HTTPProtocolType::isEncrypted("davs"));
    QVERIFY(!HTTPProtocolType::isEncrypted("webdav"));
    QVERIFY(HTTPProtocolType::isEncrypted("webdavs"));
    QVERIFY(!HTTPProtocolType::isEncrypted("http"));
    QVERIFY(HTTPProtocolType::isEncrypted("https"));
}

void HTTPProtocolTypeTest::testAsEncrypted_data()
{
    QTest::addColumn<QString>("protocolName");
    QTest::addColumn<QString>("asEncrypted");
    QTest::addColumn<QString>("asUnencrypted");

    QTest::newRow("dav") << "dav" <<"davs" << "dav";
    QTest::newRow("webdav") << "webdav" << "webdavs" << "webdav";
}

void HTTPProtocolTypeTest::testAsEncrypted()
{
    QFETCH(QString, protocolName);
    QFETCH(QString, asEncrypted);
    QFETCH(QString, asUnencrypted);
    auto pt = HTTPProtocolType{protocolName};

    QCOMPARE(pt.asEncrypted(true), asEncrypted);
    QCOMPARE(pt.asEncrypted(false), asUnencrypted);
}
void HTTPProtocolTypeTest::testChangeHttpToProtocol_data()
{
    const QUrl davU{"dav://someserver.org/res/res"},
         davsU{"davs://someserver.org/res/res"},
         webdavU{"webdav://someserver.org/res/res"},
         webdavsU{"webdavs://someserver.org/res/res"};

    QTest::addColumn<QUrl>("unencrypted");
    QTest::addColumn<QUrl>("encrypted");

    QTest::newRow("dav") << davU << davsU;
    QTest::newRow("webdav") << webdavU << webdavsU;
}

void HTTPProtocolTypeTest::testChangeHttpToProtocol()
{
    QUrl httpU{"http://someserver.org/res/res"},
         httpsU{"https://someserver.org/res/res"};
    QFETCH(QUrl, unencrypted);
    QFETCH(QUrl, encrypted);
    HTTPProtocolType pt{QString::fromLatin1(QTest::currentDataTag())};

    pt.changeHttpToProtocol(httpU);
    pt.changeHttpToProtocol(httpsU);

    QCOMPARE(httpU, unencrypted);
    QCOMPARE(httpsU, encrypted);
}

void HTTPProtocolTypeTest::testProtocolChangedToHttp_data()
{
    QTest::addColumn<QUrl>("unencrypted");
    QTest::addColumn<QUrl>("encrypted");

    QTest::newRow("dav") << QUrl{"dav://foobar"} << QUrl{"davs://foobar"};
    QTest::newRow("webdav") << QUrl{"webdav://foobar"} << QUrl{"webdavs://foobar"};
}
void HTTPProtocolTypeTest::testProtocolChangedToHttp()
{
    QFETCH(QUrl, unencrypted);
    QFETCH(QUrl, encrypted);
    QUrl httpU{"http://foobar"}, httpsU{"https://foobar"};

    QCOMPARE(HTTPProtocolType::protocolChangedToHttp(unencrypted), httpU);
    QCOMPARE(HTTPProtocolType::protocolChangedToHttp(encrypted), httpsU);
}
