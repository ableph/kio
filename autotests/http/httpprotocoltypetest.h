/*
 *    SPDX-FileCopyrightText: 2022 Alexander Busse <alex@busse.earth>
 *
 *    SPDX-License-Identifier: LGPL-2.0-or-later
 */

#ifndef HTTPPROTOCOLTYPETEST_H
#define HTTPPROTOCOLTYPETEST_H

#include <QObject>

class HTTPProtocolTypeTest : public QObject
{
    Q_OBJECT

private Q_SLOTS:

    void testIsDav();
    void testIsEncrypted();
    void testAsEncrypted_data();
    void testAsEncrypted();
    void testChangeHttpToProtocol_data();
    void testChangeHttpToProtocol();
    void testProtocolChangedToHttp_data();
    void testProtocolChangedToHttp();
};

#endif // HTTPPROTOCOLTYPETEST_H
